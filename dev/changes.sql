ALTER TABLE `place` ADD `uid` VARCHAR(32) NOT NULL AFTER `last_update`;

ALTER TABLE `place` ADD `lugares` INT(3) NOT NULL DEFAULT '1' AFTER `uid`;

ALTER TABLE `place` ADD `condiciones` TEXT NULL AFTER `lugares`;

--temporary
ALTER TABLE `place` CHANGE `uid` `uid` VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL;

ALTER TABLE `place` CHANGE `uid` `uid` VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL;


ALTER TABLE `place` ADD `user_id` INT(12) NULL AFTER `condiciones`;

ALTER TABLE `place` CHANGE `uid` `uid` VARCHAR(37) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL;

ALTER TABLE `place` ADD `precio_por_persona` DECIMAL(8,2) NOT NULL DEFAULT '0.00' AFTER `fecha_vencimiento`;