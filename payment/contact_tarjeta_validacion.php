<?php
error_reporting(E_ALL); ini_set('display_errors', 1);
session_start();
if(!$_SESSION['todos']){
  die("Wrong gateway.");
}

$_SESSION['datosTarjeta'] = $_POST;

//calcula precio por persona
//FALTA Validar el numero de niños, ver si hay descuento para niños
$total_personas = intval($_SESSION['todos']['num_adultos']) + intval($_SESSION['todos']['num_ninos']);
$total = $_SESSION['todos']['precio_por_persona'] * $total_personas;
$reference = preg_replace('/[^0-9.]+/', '', $_SESSION['todos']['viaje_uid']);
$errors = array();

if ($_SESSION['datosTarjeta']['conektaTokenId']){

    

    //echo "Todos los datos:";
    //print_r($_SESSION['todos']);
    //echo "<br>TOTAL A PAGAR:".$_SESSION['todos']['precio_por_persona']." para ".$total_personas." personas | TOTAL:".$total;

    //--------------------------------------PAGO POR TARJETA COMIENZA
    require_once("conekta/lib/Conekta.php");
    \Conekta\Conekta::setApiKey("key_yzf6njS7nHPW7ynmqHbLzg");
    \Conekta\Conekta::setApiVersion("2.0.0");

    //primero agarramos el customer
    try {
        $customer = \Conekta\Customer::create(
            array(
              "name" => $_SESSION['todos']['name'],
              "email" => $_SESSION['todos']['email'],
              "phone" => $_SESSION['todos']['phone'],
              "payment_sources" => array(
                array(
                    "type" => "card",
                    "token_id" => $_SESSION['datosTarjeta']['conektaTokenId']
                )
              )//payment_sources
            )//customer
        );
    } catch (\Conekta\ProccessingError $error){
        array_push($errors, "ERROR 101:".$error->getMesage());
    } catch (\Conekta\ParameterValidationError $error){
        array_push($errors, "ERROR 102:".$error->getMessage());
    } catch (\Conekta\Handler $error){
        array_push($errors, "ERROR 103:".$error->getMessage());
    }

    //luego intentamos la orden
    try{
      $order = \Conekta\Order::create(
        array(
          "line_items" => array(
            array(
              "name" => $_SESSION['todos']['viaje'],
              "unit_price" => ($_SESSION['todos']['precio_por_persona'] * 100),
              "quantity" => $total_personas
            )//first line_item
          ), //line_items
          
          "shipping_lines" => array(
            array(
              "amount" => 0,
               "carrier" => "Gente21"
            )
          ), //shipping_lines
          /**/
          "currency" => "MXN",
          "customer_info" => array(
            "customer_id" => $customer['id']
          ), //customer_info
          
          "shipping_contact" => array(
            "phone" => $customer['phone'],
            "receiver" => $customer['name'],
            "address" => array(
              "street1" => $_SESSION['todos']['direccion'],
              "city" => $_SESSION['todos']['city'],
              "state" => $_SESSION['todos']['city'],
              "country" => "MX",
              "postal_code" => $_SESSION['todos']['cp'],
              "residential" => true
            )//address
          ), //shipping_contact
          /**/
          "metadata" => array(
            "reference" => $reference, 
            "more_info" => "Viaje: ".$_SESSION['todos']['viaje']
            ),
          "charges" => array(
              array(
                  "payment_method" => array(
                    "payment_source_id" => $customer['default_payment_source_id'],
                    "type" => "card"
                  )//payment_method
              ) //first charge
          ) //charges
        )//order
      );

    //vamos creando un array con errores
    } catch (\Conekta\ProccessingError $error){
        array_push($errors, "ERROR 201:".$error->getMesage());
    } catch (\Conekta\ParameterValidationError $error){
        array_push($errors, "ERROR 202:".$error->getMessage());
    } catch (\Conekta\Handler $error){
        array_push($errors, "ERROR 203:".$error->getMessage());
    }

} else {
    die("Didn't get the required info to continue :O");
}


function sendMail($from, $to, $subject, $body, $total){
  $headers = "From: Charter APP para Android <".strip_tags($from).">\r\n";
  $headers .= "Reply-To: Charter APP para Android <".strip_tags($from).">\r\n";
  $headers .= "BCC: raulrodriguezarias@gmail.com, eduardo@gente21.com\r\n";
  $headers .= "MIME-Version: 1.0\r\n";
  $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

  $message = '<html><body>';
  $message .= '<h1>CONFIRMACI&Oacute;N DE RESERVACI&Oacute;N DE VIAJE</h1>';
  $message .= '<h3>'.$body.'</h3>';
  $message .= '<p>Este email es un recibo de que adquiri&oacute; el paquete: <strong>'.$_SESSION['todos']['viaje']."</strong></p>";
  $message .= '<small>Nos pondremos en contacto con usted en las pr&oacute;ximas 48 horas</small>';
  //$message .= '<img src="//css-tricks.com/examples/WebsiteChangeRequestForm/images/wcrf-header.png" alt="Website Change Request" />';
  $message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
  $message .= "<tr style='background: #eee;'><td><strong>Titular:</strong> </td><td>" . $_SESSION['todos']['name'] . "</td></tr>";
  $message .= "<tr><td><strong>Condiciones:</strong> </td><td>" . $_SESSION['todos']['condiciones'] . "</td></tr>";
  $message .= "<tr><td><strong>Total pagado:</strong> </td><td>$" . $total . "MXN (pesos mexicanos)</td></tr>";
  $message .= "<tr><td><strong>Comentarios:</strong> </td><td>" . $_SESSION['todos']['message'] . "</td></tr>";

  $message .= "</table>";
  $message .= "</body></html>"; 

  try{
    mail($to, $subject, $message, $headers);
  }  catch (Exception $e) {
    echo 'ERROR 301, no se pudo enviar la notificación: ',  $e->getMessage(), "\n";
  }
}

function sendMailSimple($from, $to, $subject, $body){
  $headers = "From: " . strip_tags($from) . "\r\n";
  $headers .= "Reply-To: ". strip_tags($from) . "\r\n";
  $headers .= "MIME-Version: 1.0\r\n";
  $headers .= "Content-Type: text/plain;\r\n";

  $message = $body;

  try{
    mail($to, $subject, $message, $headers);
  }  catch (Exception $e) {
    echo 'ERROR 302, no se pudo enviar la notificación: ',  $e->getMessage(), "\n";
  }
}

?>

<html>
    <head>
        <title>Charter Go, pago por tarjeta</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
        <link href='custom.css' rel='stylesheet' type='text/css'>

        <!-- para el datepicker -->
        <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
        <script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
        <script type="text/javascript" src="https://cdn.conekta.io/js/latest/conekta.js"></script>

    </head>
    <body>

        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">

                    <h1 style="margin-bottom: 20px;">Reservación de viaje <br><a href="#"><?php echo $_SESSION['todos']['viaje']; ?></a></h1>

                    <p class="lead">
                        <?php

                          if (count($errors) > 0){
                              echo "<strong>NO SE HA PODIDO COMPLETAR EL PAGO</strong><br><small>Por favor revise los datos de su tarjeta, al parecer son incorrectos.</small>";
                              echo "<br><a href='contact_tarjeta.php'>Intentar de nuevo</a><br><br>";
                              $errores = '';
                              $datos = '';

                              //errores
                              for ($i=0; $i < count($errors); $i++) { 
                                echo "<ul>";
                                echo "<li>".$errors[$i]."</li>";
                                echo "</ul>";
                                $errores .= $errors[$i]."\r\n";
                              }

                              //info
                              for ($i=0; $i < count($_SESSION['todos']); $i++) { 
                                $datos .= $_SESSION['todos'][$i]."\r\n";
                              }

                              //enviar notificación de pago fallido a raul
                              sendMailSimple("charter@brandsmexico.com", "raulrodriguezarias@gmail.com", "Charter APP, notificación de fallo", $errores."\r\n \r\nDATOS:\r\n".$datos);
                          } else {
                              echo "<strong>¡TE VAS DE VACACIONES!</strong><br><small>El pago se ha completado, recibirás un email con instrucciones</small>";
                              
                              //enviar recibo al comprador y a Gente21
                              sendMail('charter@brandsmexico.com', $_SESSION['todos']['email'], 'Su reservación de viaje', "Recibo de compra", $total);

                              //enviar reciboal proveedor
                              sendMail('charter@brandsmexico.com', $_SESSION['todos']['prov_email'], 'Reservación Charter APP (copia del proveedor)', "Recibo para el proveedor", $total);
                          }

                        ?>
                    </p>

                </div>
            </div>
        </div>

    </body>
</html>


    
